import trash from "../../../../../assets/trash.svg";

const Trash = () => {
	return <img src={trash} alt="trash" />;
};

export default Trash;
