import Trash from "./components/Trash";
import "./AuthorItem.css";

const AuthorItem = ({ authorList, onAddAuthor, onRemoveAuthor }) => {
	return (
		<>
			<div className="trash">
				{authorList.map((author, index) => (
					<p key={index}>
						{author.name}
						<button onClick={() => onAddAuthor(author)} type="button">
							+
						</button>
						<button onClick={() => onRemoveAuthor(author)} type="button">
							<Trash />
						</button>
					</p>
				))}
			</div>
		</>
	);
};

export default AuthorItem;
