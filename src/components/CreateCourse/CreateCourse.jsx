import Input from "../../common/Input/Input";
import Button from "../../common/Button/Button";
import AuthorItem from "./components/AuthorItem/AuthorItem";
import Trash from "./components/AuthorItem/components/Trash";

import { useNavigate } from "react-router-dom";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { useState } from "react";
import { v4 as uuidv4 } from "uuid";

import { getCourseDuration, formatCreationDate } from "../../helpers/helpers";
import { mockedAuthorsList } from "../../constants";

import styles from "./CreateCourse.module.scss";

const CreateCourse = (props) => {
	const navigate = useNavigate();
	//current author name
	const [authorName, setAuthorName] = useState("");

	//Course Authors list
	const [courseAuthors, setCourseAuthors] = useState([]);

	//Authors list
	const [allAuthors, setAllAuthors] = useState(mockedAuthorsList);

	//update the author name state
	const handleAuthorNameChange = (e) => {
		setAuthorName(e.target.value);
	};

	//add author to the Authors list
	const handleAddAuthor = () => {
		if (authorName.trim() !== "") {
			const newAuthor = {
				id: uuidv4(),
				name: authorName.trim(),
			};
			mockedAuthorsList.push(newAuthor);
			setAuthorName("");
		}
	};

	//add the author name to the Course Authors list
	const handleAuthorAdd = (author) => {
		setCourseAuthors((prevList) => [...prevList, author]);
	};

	//remove author from Course Authors list
	const handleAuthorRemoveFromCourse = (author) => {
		setCourseAuthors(courseAuthors.filter((item) => author.id !== item.id));
	};

	//remove author from Authors list
	const handleAuthorRemoveFromCourse = (author) => {
		setAllAuthors(allAuthors.filter((item) => author.id !== item.id));
		handleAuthorRemove(author);
	};

	//add course
	const initialValues = {
		title: "",
		description: "",
		duration: "",
	};

	//cancel course add
	const handleCancel = (formik) => {
		formik.resetForm();
		setAuthorName("");
	};

	const validationSchema = Yup.object({
		title: Yup.string().required("Title is required"),
		description: Yup.string().required("Description is required"),
		duration: Yup.string().required("Duration is required"),
	});

	const formattedDuration = (duration) => {
		return duration && parseInt(duration) >= 60
			? getCourseDuration(parseInt(duration))
			: `00:${duration ? duration.padStart(2, "0") : "00"}`;
	};

	const onSubmit = (values) => {
		const id = uuidv4();
		const currentDate = new Date();
		const formattedCreationDate = formatCreationDate(currentDate);
		const authorIds = courseAuthors.map((author) => author.id);

		const formattedValues = {
			id: id,
			title: values.title,
			description: values.description,
			creationDate: formattedCreationDate,
			duration: parseInt(values.duration),
			authors: authorIds,
		};

		console.log(formattedValues);
		props.addNewCourse(formattedValues);
		navigate("/courses");
	};

	return (
		<div className="create-course">
			<h2>Course edit/create page</h2>

			<Formik
				initialValues={initialValues}
				validationSchema={validationSchema}
				onSubmit={onSubmit}
			>
				{(formik) => (
					<Form>
						<div className="create-course__info">
							<h3>Main info</h3>
							<b>
								<label htmlFor="title">Title</label>
								<Input name="title" type="text" />
								<span>{formik.errors.title}</span>
								<div
									className={`create-course__info-description ${formik.errors.description && "error"}`}
								>
									<label htmlFor="description">Description</label>
									<Input
										name="description"
										placeholder="Input text"
										type="text"
										isTextArea={true}
									/>
								</div>
								<span>{formik.errors.description}</span>
							</b>
							<h3>Duration</h3>
							<b>
								<label htmlFor="duration">Duration</label>
							</b>
							<div className="create-course__info-duration">
								<Input name="duration" type="text" />
								<p>
									<b>{formattedDuration(formik.values.duration)}</b> hours
								</p>
							</div>
							<span>{formik.errors.duration}</span>
							<div className="create-course__info-authors">
								<div>
									<h3>Authors</h3>
									<b>
										<p>Author name</p>
									</b>
									<div className="create-course__info-duration">
										<Input
											name="author"
											type="text"
											value={authorName}
											onChange={handleAuthorNameChange}
										/>
										<Button
											type="button"
											buttonText="CREATE AUTHOR"
											onClick={handleAddAuthor}
										/>
									</div>
								</div>
								<div className="create-course__info-course_authors">
									<h3>Course Authors</h3>
									<p>
										{courseAuthors.map((author, index) => (
											<li key={index}>
												{author.name}
												<button
													onClick={() => handleAuthorRemoveFromCourse(author)}
												>
													<Trash />
												</button>
											</li>
										))}
										{courseAuthors.length === 0 && (
											<li>Author list is empty</li>
										)}
									</p>
								</div>
							</div>
							<h4>Authors list</h4>
							<AuthorItem
								authorList={allAuthors}
								onAddAuthor={handleAuthorAdd}
								onRemoveAuthor={handleRemoveAuthor}
							/>
						</div>
						<div className="create-course__buttons">
							<Button
								type="button"
								buttonText="CANCEL"
								onClick={() => handleCancel(formik)}
							/>
							<Button type="submit" buttonText="CREATE COURSE" />
						</div>
					</Form>
				)}
			</Formik>
		</div>
	);
};

export default CreateCourse;
