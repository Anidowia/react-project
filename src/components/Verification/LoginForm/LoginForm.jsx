import Input from "../../../common/Input/Input";
import Button from "../../../common/Button/Button";

import { Formik } from "formik";
import * as Yup from "yup";
import { Link, useNavigate } from "react-router-dom";

import "../RegistrationForm/RegistrationForm.css";

const LoginForm = () => {
	const navigate = useNavigate();

	const initialValues = {
		email: "",
		password: "",
	};

	const validationSchema = Yup.object({
		email: Yup.string().required("Email is required"),
		password: Yup.string().required("Password is required"),
	});

	const onSubmit = async (values) => {
		try {
			const response = await fetch("http://localhost:4000/login", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(values),
			});

			if (!response.ok) {
				alert("There was an error with your login: " + response.statusText);
				return;
			}

			const data = await response.json();
			const token = data.result;

			localStorage.setItem("userToken", token);
			alert("Login successful!");
			navigate("/courses");
		} catch (error) {
			alert("There was an error with the server: " + error.message);
		}
	};

	return (
		<div className="registration-form">
			<h2>Login</h2>
			<Formik
				initialValues={initialValues}
				validationSchema={validationSchema}
				onSubmit={onSubmit}
			>
				{(formik) => (
					<form
						onSubmit={formik.handleSubmit}
						className="registration-form__info"
					>
						<label htmlFor="email">Email</label>
						<Input
							id="email"
							type="email"
							{...formik.getFieldProps("email")}
							isTextArea={false}
							hasError={formik.touched.email && formik.errors.email}
						/>
						<span>{formik.errors.email}</span>
						<label htmlFor="password">Password</label>
						<Input
							id="password"
							type="password"
							{...formik.getFieldProps("password")}
							isTextArea={false}
							hasError={formik.touched.password && formik.errors.password}
						/>
						<span>{formik.errors.password}</span>
						<div className={styles["registration-form__info-button"]}>
							<Button type="submit" buttonText="LOGIN" />
							<p>
								If you have an account you may
								<Link to="/registration">
									<br />
									<b>Registration</b>
								</Link>
							</p>
						</div>
					</form>
				)}
			</Formik>
		</div>
	);
};

export default LoginForm;
