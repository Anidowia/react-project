import Input from "../../../common/Input/Input";
import Button from "../../../common/Button/Button";

import { Formik } from "formik";
import * as Yup from "yup";
import { Link, useNavigate } from "react-router-dom";

import "./RegistrationForm.css";

const RegistrationForm = () => {
	const navigate = useNavigate();

	const initialValues = {
		name: "",
		email: "",
		password: "",
	};

	const validationSchema = Yup.object({
		name: Yup.string().required("Name is required"),
		email: Yup.string().required("Email is required"),
		password: Yup.string().required("Password is required"),
	});

	const onSubmit = async (values) => {
		try {
			const response = await fetch("http://localhost:4000/register", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(values),
			});

			if (!response.ok) {
				alert(
					"There was an error with your registration: " +
						`${response.statusText}`
				);
				return;
			}
			alert("Registration successful!");
			navigate("/login");
		} catch (error) {
			alert("There was an error with the server: " + error.message);
		}
	};

	return (
		<div className="registration-form">
			<h2>Registration</h2>
			<Formik
				initialValues={initialValues}
				validationSchema={validationSchema}
				onSubmit={onSubmit}
			>
				{(formik) => (
					<form
						onSubmit={formik.handleSubmit}
						className="registration-form__info"
					>
						<label htmlFor="name">Name</label>
						<Input
							id="name"
							type="text"
							{...formik.getFieldProps("name")}
							isTextArea={false}
							hasError={formik.touched.name && formik.errors.name}
						/>
						<span>{formik.errors.name}</span>
						<label htmlFor="email">Email</label>
						<Input
							id="email"
							type="email"
							{...formik.getFieldProps("email")}
							isTextArea={false}
							hasError={formik.touched.email && formik.errors.email}
						/>
						<span>{formik.errors.email}</span>
						<label htmlFor="password">Password</label>
						<Input
							id="password"
							type="password"
							{...formik.getFieldProps("password")}
							isTextArea={false}
							hasError={formik.touched.password && formik.errors.password}
						/>
						<span>{formik.errors.password}</span>
						<div className={styles["registration-form__info-button"]}>
							<Button type="submit" buttonText="REGISTER" />
							<p>
								If you have an account you may
								<Link to="/login">
									<b> Login</b>
								</Link>
							</p>
						</div>
					</form>
				)}
			</Formik>
		</div>
	);
};

export default RegistrationForm;
