import { useParams, Link } from "react-router-dom";

import Button from "../../common/Button/Button";

import {
	getCourseDuration,
	formatCreationDate,
	formatAuthors,
} from "../../helpers/helpers";

import "./CourseInfo.css";

const CourseInfo = ({ courses, onClose }) => {
	const { courseId } = useParams();
	const course = courses.find((course) => course.id === courseId);

	return (
		<div className={styles["courses"]}>
			<h3 className={styles["courses-title"]}>{course.title}</h3>
			<div className={styles["course-card"]}>
				<div className={styles["course-info"]}>
					<h3 className={styles["course-info__title"]}> Description:</h3>
					<p className={styles["course-info__text"]}>{course.description}</p>
				</div>
				<div className="course-contacts">
					<div className="course-contacts__text">
						<p>
							<b>ID:</b> {course.id}
						</p>
						<p>
							<b>Duration:</b> {getCourseDuration(course.duration)}
						</p>
						<p>
							<b>Created:</b> {formatCreationDate(course.creationDate)}
						</p>
						<p>
							<b>Authors:</b> {formatAuthors(course.authors)}
						</p>
					</div>
				</div>
			</div>
			<div className={styles["description-button"]}>
				<Link to={"/courses"}>
					<Button buttonText="BACK" onClick={onClose} />
				</Link>
			</div>
		</div>
	);
};

export default CourseInfo;
