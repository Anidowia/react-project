import logo from "../../../../assets/logo.svg";
import "./Logo.css";

const Logo = ({ onClick }) => {
	return (
		<div onClick={onClick}>
			<img src={logo} alt="logo" />
		</div>
	);
};

export default Logo;
