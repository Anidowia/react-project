import { useState, useEffect } from "react";

import Logo from "./components/Logo/Logo";
import Button from "../../common/Button/Button";

import { useNavigate, useLocation } from "react-router-dom";

import "./Header.css";

const Header = () => {
	const navigate = useNavigate();
	const location = useLocation();
	const [isLoggedIn, setIsLoggedIn] = useState(false);

	useEffect(() => {
		const userToken = localStorage.getItem("userToken");
		setIsLoggedIn(!!userToken);
	}, []);

	const handleClick = () => {
		if (isLoggedIn) {
			localStorage.removeItem("userToken");
			navigate("/login");
		} else if (location.pathname === "/create") {
			console.log("mamaguevo");
		} else {
			navigate("/registration");
		}
	};

	const handleLogoClick = () => {
		navigate("/courses");
	};

	const isRegistrationPage = location.pathname === "/registration";
	const isLoginPage = location.pathname === "/login";

	return (
		<header className={styles["header"]}>
			<div className={styles["header-logo"]}>
				<Logo onClick={handleLogoClick} />
			</div>
			<ul className="header-menu">
				{!isRegistrationPage && !isLoginPage && (
					<Button
						buttonText={isLoggedIn ? "LOGOUT" : "LOGIN"}
						onClick={handleClick}
					/>
				)}
			</ul>
		</header>
	);
};

export default Header;
