import Button from "../../common/Button/Button";

import { useNavigate, useLocation } from "react-router-dom";

import "./EmptyCourseList.css";

const EmptyCourseList = () => {
	const navigate = useNavigate();
	const location = useLocation();

	const handleClick = () => {
		navigate("/courses/add");
	};

	const isCourseCreationPage = location.pathname === "/create";

	return (
		<div className="empty-list">
			<h2>Your List Is Empty</h2>
			<p>Please use 'Add New Course' button to add your first course</p>
			{!isCourseCreationPage && (
				<Button buttonText="ADD NEW COURSE" onClick={handleClick} />
			)}
		</div>
	);
};

export default EmptyCourseList;
