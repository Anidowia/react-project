import Courses from "./Courses/Courses";
import SearchBar from "./Courses/components/SearchBar/SearchBar";

const Main = ({ state, handleAddCourse }) => {
	return (
		<div className="bg">
			{state.length !== 0 && <SearchBar />}
			<Courses courses={state} handleAddCourse={handleAddCourse} />
		</div>
	);
};

export default Main;
