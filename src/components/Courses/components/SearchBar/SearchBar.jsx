import Button from "../../../../common/Button/Button";
import Input from "../../../../common/Input/Input";

import "./SearchBar.css";
import { Formik } from "formik";

const SearchBar = ({ onClick }) => {
	const initialValues = {
		search: "",
	};

	const handleChange = () => {
		onClick && onClick();
	};

	return (
		<div className="search-bar">
			<Formik
				initialValues={initialValues}
				validationSchema=""
				validateOnBlur={false}
				validateOnChange={false}
			>
				<Input
					className="input"
					type="text"
					value="Input text"
					onChange={handleChange}
					name="search"
				/>
			</Formik>
			<Button className="button" buttonText="SEARCH" />
		</div>
	);
};

export default SearchBar;
