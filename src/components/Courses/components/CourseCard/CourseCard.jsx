import { Link } from "react-router-dom";

import Button from "../../../../common/Button/Button";
import {
	formatCreationDate,
	getCourseDuration,
	formatAuthors,
} from "../../../../helpers/helpers";

import "./CourseCard.css";

const CourseCard = ({ course, onClick }) => {
	const handleClick = () => {
		onClick(course);
	};

	return (
		<div className="courses-page">
			<div className="course-info">
				<h3 className="course-title">{course.title}</h3>
				<p className="course-text">{course.description}</p>
			</div>
			<div className="course-details">
				<div className="course-details__texts">
					<p>
						<b>Authors:</b> {formatAuthors(course.authors)}
					</p>
					<p>
						<b>Duration:</b> {getCourseDuration(course.duration)}
					</p>
					<p>
						<b>Created:</b> {formatCreationDate(course.creationDate)}
					</p>
				</div>
				<Link to={`/courses/${course.id}`}>
					<Button
						buttonText="SHOW COURSE"
						type="button"
						onClick={handleClick}
					/>
				</Link>
			</div>
		</div>
	);
};

export default CourseCard;
