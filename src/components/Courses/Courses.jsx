import { useState } from "react";

import CourseCard from "./components/CourseCard/CourseCard";
import CourseInfo from "../CourseInfo/CourseInfo";
import EmptyCourseList from "../EmptyCourseList/EmptyCourseList";

const Courses = ({ handleAddCourse, courses }) => {
	const [shownCourse, setShownCourse] = useState(null);

	const handleCourseClick = (course) => {
		setShownCourse(course);
	};

	const handleBackClick = () => {
		setShownCourse(null);
	};

	if (courses.length === 0) {
		return <EmptyCourseList handleAddCourse={handleAddCourse} />;
	}

	return (
		<div>
			{shownCourse ? (
				<CourseInfo course={shownCourse} onClose={handleBackClick} />
			) : (
				<>
					{courses.map((course) => (
						<CourseCard
							key={course.id}
							course={course}
							onClick={handleCourseClick}
						/>
					))}
				</>
			)}
		</div>
	);
};

export default Courses;
