import "./Input.css";

import { useField } from "formik";
import PropTypes from "prop-types";

const Input = ({ hasError, isTextArea, ...props }) => {
	const [field, meta] = useField(props);

	return (
		<div className="container">
			{isTextArea ? (
				<textarea
					{...field}
					{...props}
					className={`container-textarea ${meta.error && "inputError"}`}
				></textarea>
			) : (
				<input
					placeholder="Input text"
					{...field}
					{...props}
					className={`container-input ${meta.error && "inputError"}`}
				/>
			)}
		</div>
	);
};

Input.propTypes = {
	name: PropTypes.string,
	type: PropTypes.string,
	placeholder: PropTypes.string,
	onChange: PropTypes.func,
	isTextArea: PropTypes.bool,
};

export default Input;
