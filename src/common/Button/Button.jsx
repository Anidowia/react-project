import "./Button.css";

const Button = ({ buttonText, onClick, type }) => {
	const handleClick = () => {
		onClick && onClick();
	};

	return (
		<button className="button" onClick={handleClick} type={type}>
			{buttonText}
		</button>
	);
};

export default Button;
