import Header from "./components/Header/Header";
import Main from "./components/Main";
import RegistrationForm from "./components/Verification/RegistrationForm/RegistrationForm";
import LoginForm from "./components/Verification/LoginForm/LoginForm";
import CreateCourse from "./components/CreateCourse/CreateCourse";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { useState } from "react";

import { mockedCoursesList } from "./constants";
import CourseInfo from "./components/CourseInfo/CourseInfo";

function App() {
	const [state, setState] = useState(mockedCoursesList);

	const handleAddCourse = (course) => {
		const newState = [...state, course];
		setState(newState);
	};

	return (
		<Router>
			<Header />
			<Routes>
				<Route
					path="/courses"
					element={<Main state={state} handleAddCourse={handleAddCourse} />}
				/>
				<Route path="/registration" element={<RegistrationForm />} />
				<Route path="/login" element={<LoginForm />} />
				<Route
					path="/courses/add"
					element={<CreateCourse addNewCourse={handleAddCourse} />}
				/>
				<Route
					path="/courses/:courseId"
					element={<CourseInfo courses={state} />}
				/>
			</Routes>
		</Router>
	);
}

export default App;
