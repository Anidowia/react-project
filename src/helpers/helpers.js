import { mockedAuthorsList } from "../constants";
import { useState } from "react";

export function formatAuthors(authorIds) {
	const maxLength = 32;
	let authorsText = authorIds
		.map((authorId) => {
			const author = mockedAuthorsList.find((author) => author.id === authorId);
			return author ? author.name : "Unknown";
		})
		.join(", ");

	if (authorsText.length > maxLength) {
		authorsText = authorsText.substring(0, maxLength) + "...";
	}

	return authorsText;
}

//to format date that we will receive from server
export function formatCreationDate(dateString) {
	const date = new Date(dateString);
	const day = date.getDate().toString().padStart(2, "0");
	const month = (date.getMonth() + 1).toString().padStart(2, "0");
	const year = date.getFullYear();
	return `${month}.${day}.${year}`;
}

// a helper to format course duration
export function getCourseDuration(durationInMinutes) {
	const hours = Math.floor(durationInMinutes / 60);
	const minutes = durationInMinutes % 60;

	const formatNumber = (num) => (num < 10 ? `0${num}` : num);

	const hoursFormatted =
		hours === 0 ? "" : `${formatNumber(hours)}:${formatNumber(minutes)}`;

	return `${hoursFormatted}`;
}

export function useForm() {
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [showNameError, setShowNameError] = useState(false);
	const [showEmailError, setShowEmailError] = useState(false);
	const [showPasswordError, setShowPasswordError] = useState(false);

	const handleNameChange = (event) => {
		setName(event.target.value);
		setShowNameError(false);
	};

	const handleEmailChange = (event) => {
		setEmail(event.target.value);
		setShowEmailError(false);
	};

	const handlePasswordChange = (event) => {
		setPassword(event.target.value);
		setShowPasswordError(false);
	};

	const handleSubmit = (event) => {
		event.preventDefault();

		let formIsValid = true;

		if (!name) {
			setShowNameError(true);
			formIsValid = false;
		}
		if (!email) {
			setShowEmailError(true);
			formIsValid = false;
		}
		if (!password) {
			setShowPasswordError(true);
			formIsValid = false;
		}

		if (formIsValid) {
			console.log("Name: ", name);
			console.log("Email: ", email);
			console.log("Password: ", password);
			console.log("Form submitted");
		}
	};

	return {
		name,
		email,
		password,
		showNameError,
		showEmailError,
		showPasswordError,
		handleNameChange,
		handleEmailChange,
		handlePasswordChange,
		handleSubmit,
	};
}

export function useCreateCourseForm() {
	const [title, setTitle] = useState("");
	const [description, setDescription] = useState("");
	const [duration, setDuration] = useState("");
	const [showTitleError, setShowTitleError] = useState(false);
	const [showDescriptionError, setShowDescriptionError] = useState(false);
	const [showDurationError, setShowDurationError] = useState(false);

	const handleTitleChange = (event) => {
		setTitle(event.target.value);
		setShowTitleError(false);
	};

	const handleDescriptionChange = (event) => {
		setDescription(event.target.value);
		setShowDescriptionError(false);
	};

	const handleDurationChange = (event) => {
		setDuration(event.target.value);
		setShowDurationError(false);
	};

	const handleSubmit = (event) => {
		event.preventDefault();

		let formIsValid = true;

		if (!title) {
			setShowTitleError(true);
			formIsValid = false;
		}
		if (!description) {
			setShowDescriptionError(true);
			formIsValid = false;
		}
		if (!duration) {
			setShowDurationError(true);
			formIsValid = false;
		}

		if (formIsValid) {
			console.log("Title: ", title);
			console.log("Description: ", description);
			console.log("Duration: ", duration);
			console.log("Form submitted");
		}
	};

	return {
		title,
		description,
		duration,
		showTitleError,
		showDescriptionError,
		showDurationError,
		handleTitleChange,
		handleDescriptionChange,
		handleDurationChange,
		handleSubmit,
	};
}
